import $ from 'jquery'

const $body = $('body')
const $main = $('main')

function lockBody () {
  $body.addClass('noscroll')
}

function unlockBody (scrollPosition) {
  $body.removeClass('noscroll')

  if (scrollPosition) { // top nav links should animate to position
    $main.animate({ scrollTop: scrollPosition }, 800)
  }
}

export { lockBody, unlockBody }
