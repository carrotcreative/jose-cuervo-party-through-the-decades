/* globals Vimeo */

import $ from 'jquery'
import TweenMax from 'gsap/src/uncompressed/TweenMax'
import SceneFactory from './scene-factory'
import GlobalAudioControls from './audio'
import playlists from './playlists'
import { lockBody, unlockBody } from './body-lock'
import checkAgeGate from './age_gate'
import {trackAction, trackClick} from './floodlight'
import 'lazysizes'

checkAgeGate()

$(() => {
  // Floodlight tracking for navigation links
  trackClick('#anchor-1960', { u13: '60s', cat: 'eng_c00n' })
  trackClick('#anchor-1970', { u13: '70s', cat: 'eng_c00n' })
  trackClick('#anchor-1980', { u13: '80s', cat: 'eng_c00n' })
  trackClick('#anchor-1990', { u13: '90s', cat: 'eng_c00n' })
  trackClick('#anchor-2000', { u13: '00s', cat: 'eng_c00n' })
  trackClick('#anchor-2010', { u13: '10s', cat: 'eng_c00n' })

  // floodlight tracking for content modal outbound links
  trackClick('#recipe-1960 a.logo', { cat: 'eng_c00t', u13: '60s' })
  trackClick('#recipe-1970 a.logo', { cat: 'eng_c00t', u13: '70s' })
  trackClick('#video-1970 a.logo', { cat: 'eng_c00t', u13: '70s' })
  trackClick('#recipe-1980 a.logo', { cat: 'eng_c00t', u13: '80s' })
  trackClick('#recipe-1990 a.logo', { cat: 'eng_c00t', u13: '90s' })
  trackClick('#recipe-2000 a.logo', { cat: 'eng_c00t', u13: '00s' })
  trackClick('#video-2000 a.logo', { cat: 'eng_c00t', u13: '00s' })
  trackClick('#recipe-2010 a.logo', { cat: 'eng_c00t', u13: '10s' })

  // floodlight tracking for "more recipes" links
  trackClick('.content-modal a.see-more-link', { cat: 'eng_c00s' })

  // floodlight tracking for social buttons
  trackClick('.social-wrapper .facebook, #footer .social-links .facebook', { cat: 'eng_c00r', u1: 'facebook' })
  trackClick('.social-wrapper .twitter, #footer .social-links .twitter', { cat: 'eng_c00r', u1: 'twitter' })
  trackClick('.social-wrapper .tumblr, #footer .social-links .tumblr', { cat: 'eng_c00r', u1: 'tumblr' })
  trackClick('#footer .social-links .instagram', { cat: 'eng_c00r', u1: 'instagram' })
  trackClick('#footer .social-links .youtube', { cat: 'eng_c00r', u1: 'youtube' })

  // Configure Track Lists
  const audio = new GlobalAudioControls({playlists})

  // Playlist Controls
  const muteButton = $('#mute-icon')
  muteButton.on('click', function () {
    const btn = playlistIcons.find('.playing, .paused')
    if ($(this).hasClass('muted')) {
      $(this).removeClass('muted')
      playPlaylist(btn)
    } else {
      $(this).addClass('muted')
      pausePlaylist(btn)
    }
  })

  $('.playlist-unit .prev').on('click', function () {
    const el = $(this)
    const id = el.closest('.playlist-unit').data('id')
    audio.prev(id)
  })

  $('.playlist-unit .next').on('click', function () {
    const el = $(this)
    const id = el.closest('.playlist-unit').data('id')
    audio.next(id)
  })

  const playlistIcons = $('.playlist')
  const playButtons = $('.playlist-unit .play')
  function pausePlaylist (playBtn) {
    if (playBtn && playBtn.length > 0) {
      const id = playBtn.closest('.playlist-unit').data('id')
      audio.pause(id)
      playBtn.removeClass('playing')
      playBtn.addClass('paused')
      playBtn.parents('.playlist').removeClass('active')
    }
  }

  function playPlaylist (playBtn) {
    if (playBtn && playBtn.length > 0) {
      const id = playBtn.closest('.playlist-unit').data('id')
      audio.play(id)
      if (!playBtn.hasClass('paused')) setTrackMetadata(playBtn)
      playButtons.removeClass('playing paused')
      muteButton.removeClass('muted')
      playlistIcons.removeClass('active')
      playBtn.removeClass('paused')
      playBtn.addClass('playing')
      playBtn.parents('.playlist').addClass('active')

      // track play action with floodlight, only the first time
      if (!playBtn.hasClass('tracked')) {
        const year = playBtn.closest('section').attr('id').slice(5)
        trackAction(playBtn[0], { cat: 'eng_c00p', u14: year })
        playBtn.addClass('tracked')
      }
    }
  }

  playButtons.on('click', function () {
    const btn = $(this)
    btn.hasClass('playing') ? pausePlaylist(btn) : playPlaylist(btn)
  })

  audio.players.forEach((p) => {
    p.on('next', () => {
      setTimeout(() => {
        playButtons.each((i, el) => {
          if ($(el).hasClass('playing')) setTrackMetadata($(el))
        })
      }, 10)
    })
    p.on('prev', () => {
      setTimeout(() => {
        playButtons.each((i, el) => {
          if ($(el).hasClass('playing')) setTrackMetadata($(el))
        })
      }, 10)
    })
  })

  const allTrackInfos = $('.track-info')
  function setTrackMetadata (el) {
    const meta = audio.currentTrack()
    const trackInfo = el.closest('.playlist-unit').find('.track-info')
    trackInfo.find('.title').text(`${meta.artist} - ${meta.title}`)
    allTrackInfos.removeClass('marquee')
    trackInfo.addClass('marquee')
  }

  let lastActivePlaylistId
  function saveActivePlaylistId () {
    playButtons.each((i, el) => {
      if ($(el).hasClass('playing')) {
        lastActivePlaylistId = $(el).parents('.playlist-unit').data('id')
      }
    })
  }

  // ScrollTo Navigation
  $('#nav-menu .nav-link').on('click', (e) => {
    const navOffset = 90
    const target = $(e.target).attr('href')

    if (target) {
      const scrollPosition = $('main').scrollTop() + $(target).position().top - navOffset
      unlockBody(scrollPosition)

      if ($('#nav-menu').hasClass('menu-active')) {
        $('#nav-menu').removeClass('menu-active')
      }
    }

    e.preventDefault()
  })

  // Set up video hadnlers
  const $videos = $('.video iframe')
  const vimeoPlayers = []
  $videos.each((idx, iframe) => {
    const vimeoPlayer = new Vimeo.Player(iframe)
    vimeoPlayers.push(vimeoPlayer)
    vimeoPlayer.on('play', () => {
      saveActivePlaylistId()
      $(`.playlist-unit[data-id="${lastActivePlaylistId}"] .play`).click()
    })
  })

  // Set up events for content-icons
  $('.icon-btn, .read-more-link').on('click', function () {
    const el = $(this)
    const parent = el.parent()

    // track playlist click with floodlight
    if (parent.hasClass('playlist')) return

    const $modal = $(el.data('modal-id'))
    if (!$modal) { return false }
    $modal.toggleClass('active')
    lockBody()

    // track clicks with floodlight
    const actionName = encodeURIComponent(el.next().text().trim())

    if (parent.hasClass('recipe')) {
      trackAction(this, { u12: actionName, cat: 'eng_c00o' })
    }
    if (parent.hasClass('video')) {
      trackAction(this, { u14: actionName, cat: 'eng_c00q' })
    }
    if (parent.hasClass('interview')) {
      trackAction(this, { cat: 'eng_c00u' })
    }
  })

  $('.content-modal .close-btn').on('click', function () {
    const $parent = $(this).parents('.content-modal')
    $parent.removeClass('active')
    if ($parent.hasClass('video')) {
      vimeoPlayers.forEach((v) => v.pause())
      if (lastActivePlaylistId !== null && !muteButton.hasClass('muted')) {
        $(`.playlist-unit[data-id="${lastActivePlaylistId}"] .play`).click()
        lastActivePlaylistId = null
      }
    }
    unlockBody()
  })

  $('#menu-icon').on('click', function () {
    const $navHeader = $('#nav-menu')

    if ($navHeader.hasClass('menu-active')) {
      $navHeader.removeClass('menu-active')
      unlockBody()
    } else {
      $navHeader.addClass('menu-active')
      lockBody()
    }
  })

  $('.back-to-top-icon').on('click', (e) => {
    e.preventDefault()
    $('main')
      .stop(true, true)
      .animate({
        scrollTop: 0
      },
      {
        duration: 800,
        complete: () => $('.nav-link').removeClass('selected')
      })
  })

  $('#hero .down-chevron').on('click', (e) => {
    e.preventDefault()
    const scrollTo60sPosition = $('main').scrollTop() + $('#year-1960').position().top - 90
    $('main').stop(true, true).animate({ scrollTop: scrollTo60sPosition }, 800)
  })
})

$(window).on('load', () => {
  const sceneFactory = new SceneFactory()

  TweenMax.to($('.cuervo-badge'), 4.5, { rotation: 360, repeat: -1 })
  sceneFactory.initScenesByDecade()
})

$(window).on('resize', () => {
  if ($(window).width() >= 960 && $('.content-modal.active').length === 0) {
    $('#nav-menu').removeClass('menu-active')
    unlockBody()
  }
})
