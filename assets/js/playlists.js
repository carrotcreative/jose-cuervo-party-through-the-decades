export default [
  [
    {
      title: 'Another Lover',
      artist: 'Richard Myhill',
      src: '/audio/1960/another-lover-richard-myhill.mp3'
    }, {
      title: 'She\'s The One',
      artist: 'Andrew Kingslow / Laura Sara Dowling',
      src: '/audio/1960/shes-the-one-andrew-kingslow-laura-sara-dowling.mp3'
    }, {
      title: 'Speak Softly Angel',
      artist: 'Johnnie Joe',
      src: '/audio/1960/speak-softly-angel-johnnie-joe.mp3'
    }
  ], [
    {
      title: 'Gong City',
      artist: 'Steve Ouimette, Cat Gray / John Mattox',
      src: '/audio/1970/gong-city-steve-ouimette-cat-gray-john-mattox.mp3'
    }, {
      title: 'Under Pressure',
      artist: 'Roy Benz / Johnny Beretta',
      src: '/audio/1970/under-pressure-roy-benz-johnny-beretta.mp3'
    }, {
      title: 'Crimson Moon',
      artist: 'T-Rex',
      src: '/audio/1970/crimson-moon-t-rex.mp3'
    }
  ], [
    {
      title: 'All Your Love Tonight',
      artist: 'Richard Myhill',
      src: '/audio/1980/all-your-love-tonight-richard-myhill.mp3'
    }, {
      title: 'Do It Till We Get Funky',
      artist: 'Richard Myhill, Carol Kenyon, Lance Ellington',
      src: '/audio/1980/do-it-till-we-get-funky-richard-myhill-carol-kenyon-lance-ellington.mp3'
    }, {
      title: 'I Got My Mind Made Up (You Can Get It Girl)',
      artist: 'Instant Funk',
      src: '/audio/1980/i-got-my-mind-made-up-instant-funk.mp3'
    }
  ], [
    {
      title: 'Miss Mystery Girl',
      artist: 'Chris Goulstone',
      src: '/audio/1990/miss-mystery-girl-chris-goulstone.mp3'
    }, {
      title: 'Ska Face',
      artist: 'Glenn Nishida & Mark Governor',
      src: '/audio/1990/ska-face-glenn-nishida-mark-governor.mp3'
    }, {
      title: 'Wild In The Streets',
      artist: 'Circle Jerks',
      src: '/audio/1990/wild-in-the-streets.mp3'
    }
  ], [
    {
      title: 'SuperHyphy',
      artist: 'Keak da Sneak',
      src: '/audio/2000/superhyphy-clean-keak-da-sneak.mp3'
    }, {
      title: 'Since 84',
      artist: 'Mac Dre',
      src: '/audio/2000/since-84-mac-dre.mp3'
    }, {
      title: 'Ghost Ride IT',
      artist: 'Mistah F.A.B',
      src: '/audio/2000/ghost-ride-it-mistah-f-a-b.mp3'
    }
  ], [
    {
      title: 'Milly Rock',
      artist: '2 Milly',
      src: '/audio/2010/milly-rock-clean-2milly.mp3'
    }, {
      title: 'Lemme Smang It',
      artist: 'Yung Humma feat Flynt Flossy',
      src: '/audio/2010/lemme-smang-it-yung-humma-feat-flynt-flossy.mp3'
    }, {
      title: 'Teach Me How To Dougie',
      artist: 'Cali Swag District',
      src: '/audio/2010/teach-me-how-to-dougie-clean-cali-swag-district.mp3'
    }
  ]
]
