import $ from 'jquery'
import ScrollMagic from 'scrollmagic'
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators'
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'
import TweenMax from 'gsap/src/uncompressed/TweenMax'
import TimelineMax from 'gsap/src/uncompressed/TimelineMax'

import SceneConfig from './scene-config'
import SceneAnimations from './scene-animations'

/*globals Power3 Back Linear */

export default class SceneFactory {
  constructor () {
    this.controller = new ScrollMagic.Controller()

    // Store Scenes for later modifications
    this.scenes = {}
  }

  createContentScene (opts) {
    // Create Scene with Nav Anchor Selection events by default
    const scene = new ScrollMagic.Scene(opts.scene.initOpts)
      .setTween(this.createDateLineTimeline(opts.year))
      .on('start', function (event) {
        $('.nav-link').removeClass('selected')
        if (event.state === 'BEFORE') {
          if (opts.year !== 1960) {
            $(opts.siblingSelector).addClass('selected')
          }
        } else {
          $(opts.mainSelector).addClass('selected')
        }
      })

    if (opts.scene.enableDebug) {
      scene.addIndicators()
    }

    scene.addTo(this.controller)

    return scene
  }

  createWaveformScene (opts) {
    const scene = new ScrollMagic.Scene(opts.scene.initOpts)
      .setTween(this.createWaveformTimeline(opts))

    if (opts.scene.enableDebug) {
      scene.addIndicators()
    }

    scene.addTo(this.controller)

    return scene
  }

  createLayersScene (opts) {
    const scene = new ScrollMagic.Scene(opts.scene.initOpts)

    if (opts.layerTween) {
      scene
        .on('enter', () => SceneAnimations[opts.year].play())
        .on('leave', () => SceneAnimations[opts.year].reverse())
    }

    if (opts.scene.enableDebug) {
      scene.addIndicators()
    }

    scene.addTo(this.controller)

    return scene
  }

  createDateLineTimeline (year) {
    const timeline = new TimelineMax()
    const $decade = $(`#year-${year}`)
    const tweens = [
      TweenMax.from($decade.find('.date-line_under'), 1.75, { y: '-200%', ease: Power3.easeInOut }),
      TweenMax.from($decade.find('.date-line_over'), 1.75, { y: '200%', ease: Back.easeInOut.config(1) }),
      TweenMax.from($decade.find('header'), 1.75, { y: '200%', autoAlpha: 0, ease: Back.easeInOut.config(1) }),
      TweenMax.from($decade.find('.text'), 1.75, { y: '100%', autoAlpha: 0, ease: Power3.easeInOut }),
      TweenMax.staggerFrom($decade.find('.content-icon'), 1.75, { y: '100%', autoAlpha: 0, ease: Power3.easeIn }, 0.25)
    ]

    return timeline.add(tweens)
  }

  createWaveformTimeline (opts) {
    const timeline = new TimelineMax()
    const $waveBtm = $(`${opts.mainSelector} svg.waveform-btm`)
    const $waveTop = $(`${opts.siblingSelector} svg.waveform-top`)
    let tweens = []

    tweens = tweens.concat(this.preparePathTweens($waveTop, opts.scene.waveDirection))
    tweens = tweens.concat(this.preparePathTweens($waveBtm, opts.scene.waveDirection))

    return timeline.add(tweens)
  }

  preparePathTweens ($svg, waveDirection) {
    const tweensArray = []

    $svg.find('path').each((idx, svgPath) => {
      const lineLength = svgPath.getTotalLength()
      const $path = $(svgPath)
      let tweenSettings = { strokeDashoffset: 0, ease: Linear.easeNone }
      let duration = 2

      $path.css('stroke-dasharray', lineLength)
      $path.css('stroke-dashoffset', lineLength)

      if (waveDirection === 'right-left') {
        tweenSettings = { strokeDashoffset: lineLength * 2, ease: Linear.easeNone }
      }

      if ($svg.hasClass('waveform-btm')) {
        duration = 2.25
      }

      tweensArray.push(TweenMax.to(svgPath, duration, tweenSettings))
    })

    return tweensArray
  }

  initScenesByDecade () {
    const decadeYears = [1960, 1970, 1980, 1990, 2000, 2010]

    // Loop through decades and create Scenes
    decadeYears.forEach((year, idx) => {
      const siblingYear = year === 1960 ? 1960 : year - 10

      this.scenes[year] = {
        content: this.createContentScene({
          year: year,
          mainSelector: `#anchor-${year}`,
          siblingSelector: `#anchor-${siblingYear}`,
          scene: {
            initOpts: SceneConfig[year].content,
            enableDebug: false // toggle true for scene debug markers
          }
        }),
        waveform: this.createWaveformScene({
          year: year,
          mainSelector: `#year-${year}`,
          siblingSelector: year === 1960 ? '#hero' : `#year-${siblingYear}`,
          scene: {
            initOpts: SceneConfig[year].waveform,
            waveDirection: idx % 2 === 0 ? 'right-left' : 'left-right',
            enableDebug: false
          }
        }),
        layers: this.createLayersScene({
          year: year,
          layerTween: SceneAnimations[year],
          scene: {
            initOpts: SceneConfig[year].layers,
            enableDebug: false
          }
        })
      }
    })
  }
}
