import $ from 'jquery'

const floodlightBase = {
  src: '5188901',
  type: 'josec0',
  dc_lat: '',
  dc_rdid: '',
  tag_for_child_directed_treatment: ''
}

export function trackAction (ctx, config) {
  return generateTrackFunction(config).call(ctx, { preventDefault: x => x })
}

export function trackClick (el, config) {
  $(el).on('click', generateTrackFunction(config))
}

function generateTrackFunction (config = {}) {
  config = Object.assign({}, floodlightBase, config)

  return function (e) {
    e.preventDefault()
    const url = this.getAttribute('href')
    config.ord = (Math.random() + '') * 10000000000000000

    const tag = document.createElement('iframe')
    tag.setAttribute('src', `https://${config.src}.fls.doubleclick.net/activityi${formatSemi(config)}?`)
    tag.height = '1'
    tag.width = '1'
    tag.frameborder = '0'
    tag.style = 'display: none'

    if (url) tag.onload = () => { window.location = url }
    document.body.appendChild(tag)
  }
}

function formatSemi (obj) {
  let res = ''
  for (let k in obj) { res += `;${k}=${obj[k]}` }
  return res
}
