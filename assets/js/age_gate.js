import $ from 'jquery'
import cookies from './cookies'
import {trackAction} from './floodlight'

export default function ageGate () {
  if (cookies.get('age-gate-success') === 'true') return

  $('#age-gate').show()

  $('#age-gate .no').on('click', () => {
    window.location.href = 'http://responsibility.org'
  })

  $('#age-gate .yes').on('click', function () {
    if ($('#age-gate input').is(':checked')) {
      cookies.set('age-gate-success', true, null, null, 'cuervo.com')
    }
    $('#age-gate').fadeOut()
    trackAction(this, { cat: 'cnt_c00l' })
  })
}
