import ListPlayer from 'listplayer'

class GlobalAudioControls {
  constructor (options = {}) {
    this.playlists = options.playlists
    this.players = this.playlists.map((l) => new ListPlayer({ tracks: l }))
  }

  globalPause () {
    this._previousPlayer = this._currentPlayer()
    if (!this._previousPlayer) return
    this._previousPlayer.pause()
  }

  globalResume () {
    if (!this._previousPlayer) return
    this._previousPlayer.play()
  }

  toggleMute () {
    this._currentPlayer() ? this.globalPause() : this.globalResume()
  }

  play (id) {
    this.globalPause()
    this.players[id].play()
  }

  pause (id) {
    this._previousPlayer = this.players[id]
    this.players[id].pause()
  }

  next (id) {
    this.players[id].next()
  }

  prev (id) {
    this.players[id].prev()
  }

  currentTrack () {
    return (this._currentPlayer() || this._previousPlayer).currentTrack
  }

  _currentPlayer () {
    return this.players.filter((p) => p.playing())[0]
  }
}

export default GlobalAudioControls
