import $ from 'jquery'
import TweenMax from 'gsap/src/uncompressed/TweenMax'
import TimelineMax from 'gsap/src/uncompressed/TimelineMax'

/*globals Power3 */

const tweens = {
  1970: new TimelineMax({ paused: true }),
  1980: TweenMax.from($('#year-1980 .foreground-layer'), 1.5, { x: '150%', ease: Power3.easeOut, paused: true }),
  1990: new TimelineMax({ paused: true }),
  2000: TweenMax.from($('#year-2000 .foreground-layer'), 1.5, { x: '150%', ease: Power3.easeOut, paused: true })
}

// Build 70s Timeline
const $section70s = $('#year-1970')

tweens[1970]
  .add('startClouds')
  .add([
    TweenMax.to($section70s.find('.clouds-bg'), 3, { x: 0, ease: Power3.easeOut }),
    TweenMax.to($section70s.find('.clouds-fg'), 2, { x: 0, ease: Power3.easeOut, delay: 0.5 })
  ])
  .to('#airplane', 1, { y: 10, ease: Power3.easeOut }, 'startClouds+=1')

// Wrap 90s staggered animation in Timeline for ease of use
tweens[1990]
  .add(TweenMax.staggerFrom($('#year-1990 .poster-layer'), 1.5, { y: '200%', ease: Power3.easeOut }, 0.3))

export default tweens
