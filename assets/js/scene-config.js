export default {
  1960: {
    content: {
      triggerElement: '#year-1960',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-1960',
      duration: '75%',
      offset: -500
    }
  },
  1970: {
    content: {
      triggerElement: '#year-1970',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-1970',
      duration: '75%',
      offset: -500
    },
    layers: {
      triggerElement: '#year-1970',
      triggerHook: 0.5
    }
  },
  1980: {
    content: {
      triggerElement: '#year-1980',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-1980',
      duration: '75%',
      offset: -500
    },
    layers: {
      triggerElement: '#year-1980',
      triggerHook: 0.5
    }
  },
  1990: {
    content: {
      triggerElement: '#year-1990',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-1990',
      duration: '75%',
      offset: -500
    },
    layers: {
      triggerElement: '#year-1990',
      triggerHook: 0.5
    }
  },
  2000: {
    content: {
      triggerElement: '#year-2000',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-2000',
      duration: '75%',
      offset: -500
    },
    layers: {
      triggerElement: '#year-2000',
      triggerHook: 1,
      offset: 300
    }
  },
  2010: {
    content: {
      triggerElement: '#year-2010',
      triggerHook: 1,
      offset: 150
    },
    waveform: {
      triggerElement: '#year-2010',
      duration: '75%',
      offset: -500
    }
  }
}
