const jade = require('posthtml-jade')

module.exports = {
  posthtml: (ctx) => {
    return { plugins: [
      jade({
        filename: ctx.resourcePath,
        pretty: true,
        data: require('./data.json'),
        marked: require('marked'),
        domainUrl: 'http://partythroughthedecades.cuervo.com',
        readMoreLink: (decade) => `<a class='read-more-link' data-modal-id='#read-more-${decade.year}'>Read More</a>`
      })
    ] }
  }
}
