const path = require('path')
const jade = require('posthtml-jade')
const sugarss = require('sugarss')
const cssnext = require('postcss-cssnext')
const rucksack = require('rucksack-css')
const postcssImport = require('postcss-import')
const lost = require('lost')
const normalize = require('postcss-normalize')
const inlineSvg = require('postcss-inline-svg')
const assets = require('postcss-assets')
const atVariables = require('postcss-at-rules-variables')
const conditionals = require('postcss-conditionals')
const each = require('postcss-each')

module.exports = {
  matchers: { html: '**/*.jade', css: '**/*.sss' },
  posthtml: (ctx) => {
    return { plugins: [
      jade({
        filename: ctx.resourcePath,
        pretty: true,
        data: require('./data.json'),
        marked: require('marked'),
        domainUrl: 'https://cuervo-staging.bycarrot.com',
        readMoreLink: (decade) => `<a class='read-more-link' data-modal-id='#read-more-${decade.year}'>Read More</a>`
      })
    ] }
  },
  postcss: (ctx) => {
    return {
      plugins: [
        postcssImport({ addDependencyTo: ctx }),
        each({ plugins: { beforeEach: [ atVariables, conditionals ] } }),
        cssnext,
        rucksack,
        lost,
        normalize,
        assets({
          basePath: path.join(__dirname, '/assets'),
          loadPaths: ['img/']
        }),
        inlineSvg({ path: path.join(__dirname, '/assets/img/svg') })
      ],
      parser: sugarss
    }
  },
  babel: { presets: [
    require('babel-preset-es2015'),
    require('babel-preset-stage-2')
  ] },
  ignore: ['**/layout.jade', '**/_*', '**/.DS_Store'],
  module: {
    loaders: [
      { test: /\.js/, loader: 'babel-loader!imports?define=>false', skipSpikeProcessing: true }
    ]
  }
}
