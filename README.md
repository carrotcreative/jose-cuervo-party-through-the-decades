# Jose Cuervo Microsite

Microsite for Jose Cuervo

## Setup

- make sure [node.js](http://nodejs.org) is at version >= `4`
- clone this repo down and `cd` into the folder
- run `npm install`
- run `npm run watch` (or `rootsmini watch` if you want to run your global version)

## Deployment

Hosting is through [netlify](https://netlify.com) and is hooked up to auto-deploy changes to particular branches

Env        | Branch                                                                       | URL
:--------- | :--------------------------------------------------------------------------- | :-----------------------------------
Staging    | [origin/develop](https://github.com/carrot/jose-cuervo-microsite)            | https://cuervo-staging.bycarrot.com/
Production | [origin/master](https://github.com/carrot/jose-cuervo-microsite/tree/master) | https://cuervo.bycarrot.com/
